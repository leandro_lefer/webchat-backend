package com.vector.nextchathumano;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NextChatHumanoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NextChatHumanoApplication.class, args);
	}

}
